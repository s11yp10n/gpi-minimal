# GPI-minimal version 1.0 #

A REST API server side application

### End points ###
-----

It handles **login** authentication and act as **data teller** for an authenticated user

 - `http://localhost:3000/login`

 - `http://localhost:3000/household/<username>/?access_token=<token>`


### Configuration and Installation ###
------

Check if `nodejs` and `npm` installed 

Configure your database at `GPI-minimal/app.js` change accordingly the connection string

 
```
var conString = process.env.DATABASE_URL || 'postgres://postgres:$million@localhost:5432/green_dev';
```


`cd GPI-minimal && npm install && node app.js` or `npm install && node app.js` (if you already inside)

-----

### Test ###
-----

In **curl** ` curl -vsb -I -H "username:happyUser" -H "password:simplePass" http://localhost:3000/login` and get the  token. It looks something like this :

```
* Hostname was NOT found in DNS cache
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 3000 (#0)
> GET /login HTTP/1.1
> User-Agent: curl/7.38.0
> Host: localhost:3000
> Accept: */*
> username:happyUser
> password:simplePass
> 
< HTTP/1.1 200 OK
< X-Powered-By: Express
< X-Content-Type-Options: nosniff
< Content-Type: application/json; charset=utf-8
< Content-Length: 195
< ETag: W/"c3-2eaab6fc"
< Date: Tue, 02 Jun 2015 01:22:26 GMT
< Connection: keep-alive
< 
* Connection #0 to host localhost left intact
{"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOjIsImV4cCI6MTQzMzI5NDU0Njk2Mn0.bV3x6eKoDkprvF70ONgjq_YIxLKvDLj_ANKK1JQOV0Q","expires":1433294546962,"user":{"id":2,"username":"happyUser"}}

```
### Fetch Own Data ###

In **postman** view the data for the above authenticated user with `http://localhost:3000/household/happyUser?access_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOjIsImV4cCI6MTQzMzI5NDU0Njk2Mn0.bV3x6eKoDkprvF70ONgjq_YIxLKvDLj_ANKK1JQOV0Q`