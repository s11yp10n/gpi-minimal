var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var pg = require('pg');
var jwt = require('jwt-simple');
var moment = require('moment');
var url = require('url');

var conString = process.env.DATABASE_URL || 'postgres://postgres:$million@localhost:5432/green_dev';
var userInfoQuery = 'SELECT * FROM users where username=$1';
var userNameQuery = 'SELECT username FROM users where id=$1';
var tokenUpdateQuery = 'UPDATE users SET auth_token=$1 where username=$2';
var userTimeSeriesQuery = 'SELECT ht.* FROM users u LEFT OUTER JOIN "user_household" uh  \
     on u.id = uh.user_id LEFT OUTER JOIN household_timeseries ht \
     on uh.household_id = ht.household_id where u.username=$1';

app.set('jwtTokenSecret', 'greenelyws');

app.get('/', function(req, res) {
    res.send('Green API Services : handles - 1./login 2. /household/:username with right access token');
});

app.get('/login', bodyParser.urlencoded({extended:false}), function (req, res) {
    var username = req.headers.username, password = req.headers.password;

    if (username && password) {
        pg.connect(conString,function(err, client,done){
            client.query(userInfoQuery, [username], function(err, result){
                if(err)
                    return console.error('could not connect to postgres', err);

                var user = result.rows[0];
                if(isAuthenticated = user.password === password){
                    var user_basic = {id: user.id, username: user.username };
                    var expires = moment().add(1, 'days').valueOf();
                    var token = jwt.encode(
                        {
                            iss: user.id,
                            exp: expires
                        },
                        app.get('jwtTokenSecret')
                    );
                    client.query(tokenUpdateQuery,[token,username],function(err){
                        if(err) return console.error('could not connect to postgres', err);
                    });
                    res.jsonp({
                        token: token,
                        expires: expires,
                        user: user_basic
                    });
                }
            });
            done();
        });
    }
});

app.get('/household/:userName', bodyParser.urlencoded({extended:false}), function (req, res, next){
    var parsed_url = url.parse(req.url, true);
    var token = (req.body && req.body.access_token) || parsed_url.query.access_token || req.headers["x-access-token"];

    if (token) {
        pg.connect(conString,function(err, client,done){
            var attemptUser = req.params.userName;
            var decodedUserToken = jwt.decode(token, app.get('jwtTokenSecret'));
            var decodedUserId = decodedUserToken.iss;

            if (decodedUserToken.exp <= Date.now()) res.end('Access token has expired', 400);

            client.query(userNameQuery, [decodedUserId], function(err, result){
                if(err)
                    return console.error('could not connect to postgres', err);

                var decodedUserName = result.rows[0].username;
                var timeSeries = {};

                if (attemptUser !== decodedUserName) res.send("You are not the owner of the requested data !");
                else if(attemptUser === decodedUserName){
                    client.query(userTimeSeriesQuery,[attemptUser], function(err, result){
                        if(err)
                            return console.error('could not connect to postgres', err);

                        timeSeries = result.rows;
                        res.json({
                            data: timeSeries
                        });
                        console.log("First household id: " + result.rows[0].household_id + " Last household id: " + result.rows[timeSeries.length -1].household_id );
                    });
                }
            });
            done();
        });

    } else next();
});

app.set('port', process.env.PORT || 3000);
var server = app.listen(app.get('port'), function() {console.log('Server is listening on port %s' ,server.address().port);});
//module.exports = app; // uncomment when project grows and other modules added